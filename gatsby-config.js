module.exports = {
    siteMetadata: {
        siteUrl: 'https://perspective4you.com',
        title: 'Perspective4You',
        description: 'Suntem o companie specializata si experimentata in recrutarea si Plasarea Fortei de Munca in strainatate. Venim in sprijinul tuturor persoanelor aflate in cautarea unui loc de munca, oferind posturi disponibile intr-o varietate larga de segmente de activitate, la angajatori de top. Suntem responsabili de tot procesul angajarii, de la analiza si plasarea clientului, la colaborare si comunicare constanta cu angajatorul pentru asiguarea unor conditii optime de munca.',
    },
    plugins: [
        'gatsby-plugin-netlify-cms',
        'gatsby-plugin-sass',
        'gatsby-plugin-sharp',
        'gatsby-plugin-react-helmet',
        'gatsby-plugin-sitemap',
        'gatsby-plugin-offline',
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                icon: 'src/other_media/favicon.svg',
            },
        },
        'gatsby-transformer-remark',
        'gatsby-plugin-mdx',
        'gatsby-transformer-sharp',
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'media',
                path: './src/other_media/',
            },
            __key: 'media',
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'images',
                path: './src/images/',
            },
            __key: 'images',
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'pages',
                path: './src/pages/',
            },
            __key: 'pages',
        },
        {
            resolve: `gatsby-plugin-intl`,
            options: {
                // language JSON resource path
                path: `${__dirname}/src/translation`,
                // supported language
                languages: ['ge', 'ro', 'en'],
                // language file path
                defaultLanguage: `ge`,
                // option to redirect to `/en` when connecting `/`
                redirect: true,
            },
        },
        {
            resolve: "gatsby-plugin-cookiebot",
            options: {
                cookiebotId: "3a42654c-1430-4985-a0d9-e5daf2aa43ab", // Required. Site's Cookiebot ID.
                manualMode: true, // Optional. Turns on Cookiebot's manual mode. Defaults to false.
                blockGtm: true, //  Optional. Skip blocking of GTM. Defaults to true if manualMode is set to true.
                includeInDevelopment: true, // Optional. Enables plugin in development. Will cause gatsby-plugin-google-tagmanager to thrown an error when pushing to dataLayer. Defaults to false.
                pluginDebug: true, // Optional. Debug mode for plugin development. Defaults to false.
            },
        },
    ],
};