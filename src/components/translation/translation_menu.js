import React from 'react';
import styles from './translation.module.scss';
import { IntlContextConsumer, changeLocale } from 'gatsby-plugin-intl';
import Flag from 'react-world-flags';

const Translation = () => {
  const languageName = {
    en: 'US',
    ro: 'RO',
    ge: 'DE',
  };

  return (
    <div className={styles.menu}>
      <IntlContextConsumer>
        {({ languages, language: currentLocale }) =>
          languages.map((language) => (
            <button
              key={language}
              onClick={() => changeLocale(language)}
              className={
                currentLocale === language
                  ? styles.currentLangOption
                  : styles.langOption
              }
            >
              <Flag code={languageName[language]} />
            </button>
          ))
        }
      </IntlContextConsumer>
    </div>
  );
};

export default Translation;
