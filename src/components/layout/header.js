import React from 'react';
import styles from './layout.module.scss';
import Translation from '../translation/translation_menu';
import Img from 'gatsby-image';
import { useImage } from '../../hooks/media';
import { FormattedMessage, Link } from 'gatsby-plugin-intl';
import {
  FaHome,
  FaEye,
  FaLocationArrow,
  FaBriefcase,
  FaDoorOpen,
  FaListUl,
} from 'react-icons/fa';
import { IoMdMenu } from 'react-icons/io';

import { Dropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.upperHeader}>
        <Dropdown className={styles.phoneMenu}>
          <Dropdown.Toggle variant='success' id='dropdown-basic'>
            <IoMdMenu />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Link
              activeClassName={styles.activeLink}
              className={styles.link}
              to='/'
            >
              <FaHome /> <FormattedMessage id='home' />
            </Link>
            <Link
              activeClassName={styles.activeLink}
              className={styles.link}
              to='/about/'
            >
              <FaEye /> <FormattedMessage id='aboutus' />
            </Link>
            <Link
              activeClassName={styles.activeLink}
              className={styles.link}
              to='/working-fields/'
            >
              <FaListUl /> <FormattedMessage id='working_fields' />
            </Link>
            {/* <Link
              activeClassName={styles.activeLink}
              className={styles.link}
              to='/active-jobs/'
            >
              <FaBriefcase /> <FormattedMessage id='active_jobs' />
            </Link> */}
            <Link
              activeClassName={styles.activeLink}
              className={styles.link}
              to='/ongoing-projects/'
            >
              <FaDoorOpen /> <FormattedMessage id='projects' />
            </Link>
            <Link
              activeClassName={styles.activeLink}
              className={styles.link}
              to='/contact/'
            >
              <FaLocationArrow /> <FormattedMessage id='contact' />
            </Link>
            <Translation />
          </Dropdown.Menu>
        </Dropdown>
        <Img
          fadeIn={false}
          loading={'eager'}
          className={styles.image}
          fluid={useImage('logo-trans.png')}
          alt='logo'
        />
        <div className={styles.media_logos}>
          <a
            target='_blank'
            className={styles.call}
            rel='noreferrer'
            href='tel:+40 740 766 602'
          >
            <FormattedMessage id='call' />
          </a>

          <a href='tel:live:perspective44you' target='_blank' rel='noreferrer'>
            <img
              alt='skype'
              src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDI5MS4zNjQgMjkxLjM2NCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMjgyLjk2NiwxNzYuNjEyYzIuMDk0LTkuNDk1LDMuMjA0LTE5LjMwOSwzLjIwNC0yOS40MDVjMC03NS42NjEtNjIuMTk3LTEzNy4wMTEtMTM4Ljk0MS0xMzcuMDExICAgYy04LjA5MywwLTE2LjAzMiwwLjY5Mi0yMy43NTIsMi4wMDNDMTExLjA2MSw0LjQ3OSw5Ni4zNjcsMCw4MC42MTgsMEMzNi4xMDEsMCwwLjAzMiwzNS41NzcsMC4wMzIsNzkuNDc1ICAgYzAsMTQuNjU3LDQuMDUxLDI4LjM4NSwxMS4wNyw0MC4xOTNjLTEuODQ4LDguODk0LTIuODEzLDE4LjEwNy0yLjgxMywyNy41MzljMCw3NS42ODgsNjIuMTk3LDEzNC43NTMsMTM4LjkzMiwxMzQuNzUzICAgYzguNjk0LDAsMTcuMTk3LTAuNzkyLDI1LjQzNi0yLjI5NGMxMS4zNTIsNi4wMDgsMjQuMzE2LDExLjY5OCwzOC4wOTksMTEuNjk4YzQ0LjUwOCwwLDgwLjU3Ny0zNS41NjgsODAuNTc3LTc5LjQ2NiAgIEMyOTEuMzMzLDE5OS4yMzUsMjg4LjMxOSwxODcuMjQ1LDI4Mi45NjYsMTc2LjYxMnoiIGZpbGw9IiNmZjY2MDIiIGRhdGEtb3JpZ2luYWw9IiMwMGFmZjAiPjwvcGF0aD4KCTxwYXRoIHN0eWxlPSIiIGQ9Ik0yMTAuNiwyMDMuMTMxYy01LjM2Miw3LjYyLTEzLjIxLDEzLjYxLTIzLjU0MiwxNy45MTZjLTEwLjMzMyw0LjM0Mi0yMi41NjgsNi40OTEtMzYuNzI0LDYuNDkxICAgYy0xNi45NzgsMC0zMC45OC0yLjk1LTQyLjAyMy04Ljg1OGMtNy44MTEtNC4yNDItMTQuMTc0LTkuOTIzLTE5LjA3Mi0xNy4wMDZjLTQuODgtNy4wODMtNy4zMjgtMTMuOTgzLTcuMzI4LTIwLjY5MyAgIGMwLTMuODc4LDEuNDY2LTcuMjEsNC4zOTctOS45NjljMi45MzEtMi43OTUsNi42NzMtNC4xNjksMTEuMTk4LTQuMTY5YzMuNjc4LDAsNi44LDEuMDY1LDkuMzMxLDMuMjQxICAgYzIuNTQ5LDIuMTU4LDQuNzI1LDUuMzUzLDYuNTA5LDkuNjA0YzIuMTk0LDUuMDUzLDQuNTUyLDkuMjY4LDcuMTEsMTIuNjQ1YzIuNTMxLDMuMzc3LDYuMTE4LDYuMTgxLDEwLjcyNCw4LjM3NSAgIGM0LjYyNSwyLjE3NiwxMC42OTcsMy4yODYsMTguMTk4LDMuMjg2YzEwLjMyNCwwLDE4LjcxNy0yLjIwMywyNS4xODEtNi42MzdjNi40NDUtNC40MTUsOS42ODYtOS45NTksOS42ODYtMTYuNTc4ICAgYzAtNS4yNDQtMS43NDgtOS41MzItNS4yMjYtMTIuNzgyYy0zLjQ4Ny0zLjI3Ny03Ljk4NC01Ljc4MS0xMy41MDEtNy41MDFjLTUuNTA4LTEuNzM5LTEyLjg4Mi0zLjU3OC0yMi4xMzEtNS41MTcgICBjLTEyLjM1NC0yLjY0OS0yMi43MDUtNS43ODEtMzEuMDI1LTkuMzMxYy04LjMzLTMuNTc4LTE0Ljk1Ny04LjQzLTE5LjgzNy0xNC41NzVjLTQuODk4LTYuMTcyLTcuMzM4LTEzLjgxOS03LjMzOC0yMi45NSAgIGMwLTguNzIxLDIuNTc2LTE2LjQ1LDcuNzQ3LTIzLjIxNGM1LjE2Mi02Ljc3MywxMi42MTgtMTEuOTcxLDIyLjQxMy0xNS42MTNjOS43NzctMy42NDEsMjEuMjg1LTUuNDUzLDM0LjQ5NC01LjQ1MyAgIGMxMC41NiwwLDE5LjcsMS4yMTEsMjcuNDExLDMuNjA1YzcuNzExLDIuNDIyLDE0LjA5Myw1LjYyNiwxOS4xNzIsOS42MjNjNS4wODksMy45OTcsOC44MTIsOC4xODQsMTEuMTUyLDEyLjU4MSAgIGMyLjM0OSw0LjM3LDMuNTIzLDguNjg1LDMuNTIzLDEyLjgzNmMwLDMuODE0LTEuNDU3LDcuMjc0LTQuMzg4LDEwLjMyNGMtMi45MzEsMy4wNTktNi41OTEsNC41OTctMTAuOTcsNC41OTcgICBjLTMuOTk3LDAtNy4wMjgtMC45MjktOS4xMDQtMi43NThjLTIuMDc2LTEuODM5LTQuMzI0LTQuODQzLTYuNzM3LTkuMDIyYy0zLjE0MS01Ljk3Mi02Ljg4Mi0xMC42MzMtMTEuMjctMTMuOTc0ICAgYy00LjM4OC0zLjM1LTExLjQxNi01LjAyNS0yMS4xMjEtNS4wMjVjLTguOTg1LDAtMTYuMjUsMS44MDMtMjEuNzY3LDUuNDUzYy01LjUxNywzLjYyMy04LjI4NCw4LjAxMS04LjI4NCwxMy4xMTggICBjMCwzLjE1LDAuOTU2LDUuODk5LDIuODIyLDguMjAyYzEuODY2LDIuMzIxLDQuNDUyLDQuMjc5LDcuNzU2LDUuOTI3YzMuMjY4LDEuNjU3LDYuNiwyLjk1LDkuOTU5LDMuOTA1ICAgYzMuMzU5LDAuOTM4LDguOTIyLDIuMjk0LDE2LjY3OCw0LjA5N2M5LjY4NiwyLjA4NSwxOC40NzEsNC4zOTcsMjYuMzM3LDYuOTFjNy44NTcsMi41MjIsMTQuNTU3LDUuNTksMjAuMDc0LDkuMTY3ICAgYzUuNTA4LDMuNjA1LDkuODIzLDguMTc1LDEyLjkwOSwxMy42OTJjMy4wODYsNS40OTksNC42NDMsMTIuMjQ0LDQuNjQzLDIwLjIzOEMyMTguNjM5LDE4Ni44OTksMjE1Ljk0NSwxOTUuNTAyLDIxMC42LDIwMy4xMzF6IiBmaWxsPSIjZmZmZmZmIiBkYXRhLW9yaWdpbmFsPSIjZmZmZmZmIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg=='
            />
          </a>
          {/* <a href='https://www.google.com' target='_blank' rel='noreferrer'>
            <img
              alt='google'
              src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDI5MS4zMiAyOTEuMzIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggc3R5bGU9IiIgZD0iTTE0NS42NiwwYzgwLjExMywwLDE0NS42Niw2NS41NDcsMTQ1LjY2LDE0NS42NnMtNjUuNTQ3LDE0NS42Ni0xNDUuNjYsMTQ1LjY2UzAsMjI1Ljc3MiwwLDE0NS42NiAgIFM2NS41NDcsMCwxNDUuNjYsMHoiIGZpbGw9IiNmZjY2MDIiIGRhdGEtb3JpZ2luYWw9IiNkZDRiMzkiPjwvcGF0aD4KCTxnPgoJCTxwb2x5Z29uIHN0eWxlPSIiIHBvaW50cz0iMjE5LjQsMTIxLjA3OSAyMTkuNCwxMzcuNDY2IDIzNS43ODYsMTM3LjQ2NiAyMzUuNzg2LDE1My44NTMgMjE5LjQsMTUzLjg1MyAyMTkuNCwxNzAuMjQgICAgIDIwMy4wMTMsMTcwLjI0IDIwMy4wMTMsMTUzLjg1MyAxODYuNjI2LDE1My44NTMgMTg2LjYyNiwxMzcuNDY2IDIwMy4wMTMsMTM3LjQ2NiAyMDMuMDEzLDEyMS4wNzkgICAiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiNmZmZmZmYiPjwvcG9seWdvbj4KCQk8cGF0aCBzdHlsZT0iIiBkPSJNNzEuOTE5LDE4Ni42MjZjLTIxLjg0OS0yMS44NDktMjEuODQ5LTYwLjA4NSwwLjkxLTgxLjkzMyAgICBjMTYuMzg3LTE2LjM4Nyw0MC4wNTYtMjAuMDI4LDYwLjk5NS0xMS44MzVsOC4xOTMsNC41NTJsOS4xMDQsNi4zNzNsLTE0LjU2NiwxNC41NjZsLTUuNDYyLTMuNjQxICAgIGMtMTIuNzQ1LTguMTkzLTMwLjk1My02LjM3My00Mi43ODcsNS40NjJjLTEzLjY1NiwxMy42NTYtMTQuNTY2LDM4LjIzNiwwLDUyLjgwMmMxMy42NTYsMTQuNTY2LDM4LjIzNiwxNC41NjYsNTAuOTgxLDAgICAgYzMuNjQxLTMuNjQxLDcuMjgzLTkuMTA0LDguMTkzLTE0LjU2NnYtMS44MjFoLTMyLjc3M3YtMTguMjA3aDUxLjg5MWwwLjkxLDQuNTUydjEyLjc0NWMtMC45MSwxMS44MzUtNS40NjIsMjMuNjctMTMuNjU2LDMxLjg2MyAgICBDMTMyLjAwNCwyMDguNDc1LDkzLjc2OCwyMDguNDc1LDcxLjkxOSwxODYuNjI2eiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iI2ZmZmZmZiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4='
            />
          </a> */}
          <a
            href='https://www.facebook.com/contracte.germaniacristina/'
            target='_blank'
            rel='noreferrer'
          >
            <img
              alt='facebook'
              src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDI5MS4zMTkgMjkxLjMxOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMTQ1LjY1OSwwYzgwLjQ1LDAsMTQ1LjY2LDY1LjIxOSwxNDUuNjYsMTQ1LjY2YzAsODAuNDUtNjUuMjEsMTQ1LjY1OS0xNDUuNjYsMTQ1LjY1OSAgIFMwLDIyNi4xMDksMCwxNDUuNjZDMCw2NS4yMTksNjUuMjEsMCwxNDUuNjU5LDB6IiBmaWxsPSIjZmY2NjAyIiBkYXRhLW9yaWdpbmFsPSIjM2I1OTk4IiBjbGFzcz0iIj48L3BhdGg+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMTYzLjM5NCwxMDAuMjc3aDE4Ljc3MnYtMjcuNzNoLTIyLjA2N3YwLjFjLTI2LjczOCwwLjk0Ny0zMi4yMTgsMTUuOTc3LTMyLjcwMSwzMS43NjNoLTAuMDU1ICAgdjEzLjg0N2gtMTguMjA3djI3LjE1NmgxOC4yMDd2NzIuNzkzaDI3LjQzOXYtNzIuNzkzaDIyLjQ3N2w0LjM0Mi0yNy4xNTZoLTI2Ljgxdi04LjM2NiAgIEMxNTQuNzkxLDEwNC41NTYsMTU4LjM0MSwxMDAuMjc3LDE2My4zOTQsMTAwLjI3N3oiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiNmZmZmZmYiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+'
            />
          </a>
          <a
            href='https://www.facebook.com/Contracte-Germania-Cristina-101694511944590'
            target='_blank'
            rel='noreferrer'
          >
            <img
              alt='facebook'
              src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDI5MS4zMTkgMjkxLjMxOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMTQ1LjY1OSwwYzgwLjQ1LDAsMTQ1LjY2LDY1LjIxOSwxNDUuNjYsMTQ1LjY2YzAsODAuNDUtNjUuMjEsMTQ1LjY1OS0xNDUuNjYsMTQ1LjY1OSAgIFMwLDIyNi4xMDksMCwxNDUuNjZDMCw2NS4yMTksNjUuMjEsMCwxNDUuNjU5LDB6IiBmaWxsPSIjZmY2NjAyIiBkYXRhLW9yaWdpbmFsPSIjM2I1OTk4IiBjbGFzcz0iIj48L3BhdGg+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMTYzLjM5NCwxMDAuMjc3aDE4Ljc3MnYtMjcuNzNoLTIyLjA2N3YwLjFjLTI2LjczOCwwLjk0Ny0zMi4yMTgsMTUuOTc3LTMyLjcwMSwzMS43NjNoLTAuMDU1ICAgdjEzLjg0N2gtMTguMjA3djI3LjE1NmgxOC4yMDd2NzIuNzkzaDI3LjQzOXYtNzIuNzkzaDIyLjQ3N2w0LjM0Mi0yNy4xNTZoLTI2Ljgxdi04LjM2NiAgIEMxNTQuNzkxLDEwNC41NTYsMTU4LjM0MSwxMDAuMjc3LDE2My4zOTQsMTAwLjI3N3oiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiNmZmZmZmYiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+'
            />
          </a>
          {/* <a href='https://www.linkedin.com' target='_blank' rel='noreferrer'>
            <img
              alt='linkedin'
              src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDI5MS4zMTkgMjkxLjMxOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMTQ1LjY1OSwwYzgwLjQ1LDAsMTQ1LjY2LDY1LjIxOSwxNDUuNjYsMTQ1LjY2cy02NS4yMSwxNDUuNjU5LTE0NS42NiwxNDUuNjU5UzAsMjI2LjEsMCwxNDUuNjYgICBTNjUuMjEsMCwxNDUuNjU5LDB6IiBmaWxsPSIjZmY2NjAyIiBkYXRhLW9yaWdpbmFsPSIjMGU3NmE4Ij48L3BhdGg+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNODIuMDc5LDIwMC4xMzZoMjcuMjc1di05MC45MUg4Mi4wNzlWMjAwLjEzNnogTTE4OC4zMzgsMTA2LjA3NyAgIGMtMTMuMjM3LDAtMjUuMDgxLDQuODM0LTMzLjQ4MywxNS41MDR2LTEyLjY1NEgxMjcuNDh2OTEuMjFoMjcuMzc1di00OS4zMjRjMC0xMC40MjQsOS41NS0yMC41OTMsMjEuNTEyLTIwLjU5MyAgIHMxNC45MTIsMTAuMTY5LDE0LjkxMiwyMC4zMzh2NDkuNTdoMjcuMjc1di01MS42QzIxOC41NTMsMTEyLjY4NiwyMDEuNTg0LDEwNi4wNzcsMTg4LjMzOCwxMDYuMDc3eiBNOTUuNTg5LDEwMC4xNDEgICBjNy41MzgsMCwxMy42NTYtNi4xMTgsMTMuNjU2LTEzLjY1NlMxMDMuMTI3LDcyLjgzLDk1LjU4OSw3Mi44M3MtMTMuNjU2LDYuMTE4LTEzLjY1NiwxMy42NTZTODguMDUxLDEwMC4xNDEsOTUuNTg5LDEwMC4xNDF6IiBmaWxsPSIjZmZmZmZmIiBkYXRhLW9yaWdpbmFsPSIjZmZmZmZmIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg=='
            />
          </a>
          <a href='https://www.twitter.com' target='_blank' rel='noreferrer'>
            <img
              alt='twitter'
              src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDI5MS4zMTkgMjkxLjMxOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMTQ1LjY1OSwwYzgwLjQ1LDAsMTQ1LjY2LDY1LjIxOSwxNDUuNjYsMTQ1LjY2YzAsODAuNDUtNjUuMjEsMTQ1LjY1OS0xNDUuNjYsMTQ1LjY1OSAgIFMwLDIyNi4xMDksMCwxNDUuNjZDMCw2NS4yMTksNjUuMjEsMCwxNDUuNjU5LDB6IiBmaWxsPSIjZmY2NjAyIiBkYXRhLW9yaWdpbmFsPSIjMjZhNmQxIiBjbGFzcz0iIj48L3BhdGg+Cgk8cGF0aCBzdHlsZT0iIiBkPSJNMjM2LjcyNCw5OC4xMjljLTYuMzYzLDIuNzQ5LTEzLjIxLDQuNTk3LTIwLjM5Miw1LjQzNWM3LjMzOC00LjI3LDEyLjk2NC0xMS4wMTYsMTUuNjEzLTE5LjA3MiAgIGMtNi44NjQsMy45Ni0xNC40NTcsNi44MjgtMjIuNTUsOC4zNjZjLTYuNDczLTYuNjkxLTE1LjY5NS0xMC44Ny0yNS45MDktMTAuODdjLTE5LjU5MSwwLTM1LjQ4NiwxNS40MTMtMzUuNDg2LDM0LjQzOSAgIGMwLDIuNzA0LDAuMzEsNS4zMzUsMC45MTksNy44NTdjLTI5LjQ5Ni0xLjQzOC01NS42Ni0xNS4xNTgtNzMuMTU3LTM1Ljk5NmMtMy4wNTksNS4wODktNC44MDcsMTAuOTk3LTQuODA3LDE3LjMxNSAgIGMwLDExLjk0NCw2LjI2MywyMi41MDQsMTUuNzg2LDI4LjY2OGMtNS44MjYtMC4xODItMTEuMjg5LTEuNzIxLTE2LjA4Ni00LjMxNXYwLjQzN2MwLDE2LjY5NiwxMi4yMzUsMzAuNjE2LDI4LjQ3NiwzMy43ODQgICBjLTIuOTc3LDAuNzgzLTYuMTA5LDEuMjExLTkuMzUsMS4yMTFjLTIuMjg1LDAtNC41MDYtMC4yMDktNi42NzMtMC42MTljNC41MTUsMTMuNjkyLDE3LjYyNSwyMy42NTEsMzMuMTY1LDIzLjkyNSAgIGMtMTIuMTUzLDkuMjQ5LTI3LjQ1NywxNC43NDgtNDQuMDg5LDE0Ljc0OGMtMi44NjgsMC01LjY5LTAuMTY0LTguNDc2LTAuNDgyYzE1LjcyMiw5Ljc3NywzNC4zNjcsMTUuNDg1LDU0LjQyMiwxNS40ODUgICBjNjUuMjkyLDAsMTAwLjk5Ny01Mi41MSwxMDAuOTk3LTk4LjAyOWwtMC4xLTQuNDYxQzIyNS45NDUsMTExLjExMSwyMzEuOTYzLDEwNS4wNDgsMjM2LjcyNCw5OC4xMjl6IiBmaWxsPSIjZmZmZmZmIiBkYXRhLW9yaWdpbmFsPSIjZmZmZmZmIiBjbGFzcz0iIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg=='
            />
          </a> */}

          <Translation />
        </div>
      </div>
      <div className={styles.menu}>
        <div className={styles.navigation}>
          <Link
            activeClassName={styles.activeLink}
            className={styles.link}
            to='/'
          >
            <FaHome /> <FormattedMessage id='home' />
          </Link>
          <Link
            activeClassName={styles.activeLink}
            className={styles.link}
            to='/about/'
          >
            <FaEye /> <FormattedMessage id='aboutus' />
          </Link>
          <Link
            activeClassName={styles.activeLink}
            className={styles.link}
            to='/working-fields/'
          >
            <FaListUl /> <FormattedMessage id='working_fields' />
          </Link>
          {/* <Link
            activeClassName={styles.activeLink}
            className={styles.link}
            to='/active-jobs/'
          >
            <FaBriefcase /> <FormattedMessage id='active_jobs' />
          </Link> */}
          <Link
            activeClassName={styles.activeLink}
            className={styles.link}
            to='/ongoing-projects/'
          >
            <FaDoorOpen /> <FormattedMessage id='projects' />
          </Link>
        </div>
        <div className={styles.contact}>
          <Link
            activeClassName={styles.activeLink}
            className={styles.link}
            to='/contact/'
          >
            <FaLocationArrow /> <FormattedMessage id='contact' />
          </Link>
        </div>
      </div>
    </div>
  );
};
export default Header;
