// import { Link } from 'gatsby';
import React from 'react';
// import {
//   facebook,
//   twitter,
//   google_plus,
//   skype,
//   linkedin,
// } from '../../other_media';
import { FaMapMarkerAlt, FaRegClock } from 'react-icons/fa';
import Img from 'gatsby-image';
import { useImage } from '../../hooks/media';

import styles from './layout.module.scss';
import { FormattedMessage } from 'gatsby-plugin-intl';

const Footer = () => {
  return (
    <div className={styles.footer}>
      <div className={styles.contentFooter}>
        <div className={styles.main}>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.logo}
            fluid={useImage('persp2b.png')}
            alt='logo'
          />
          <div className={styles.media}>
            <h3>
              <FaMapMarkerAlt />
              <FormattedMessage id='findUs' />
            </h3>
            <a href='https://www.facebook.com/contracte.germaniacristina/'>
              <FormattedMessage id='facebook' />
            </a>
            <a href='https://www.facebook.com/Contracte-Germania-Cristina-101694511944590'>
              <FormattedMessage id='facebookPage' />
            </a>
            <a href='tel:live:perspective44you'>
              <FormattedMessage id='skype' />
            </a>
          </div>

          <div className={styles.navigation}>
            <h3>
              <FaRegClock />
              <FormattedMessage id='program' />
            </h3>
            <p>
              <FormattedMessage id='phoneLine' />
            </p>
            <p>
              <FormattedMessage id='week' />
            </p>
            <p>
              <FormattedMessage id='saturday' />
            </p>
            <p>
              <FormattedMessage id='sunday' />
            </p>
          </div>
        </div>
        <div className={styles.contact}>
          <h3>
            <FormattedMessage id='questions' />
          </h3>
          <p>
            <FormattedMessage id='contactViaMail' />
          </p>
          <a
            href='mailto:office@perspective4you.com
'
          >
            office@perspective4you.com
          </a>
          <p>
            <FormattedMessage id='contactViaPhone' />
          </p>
          <a href='tel:+40 740 766 602'>+40 740 766 602</a>
          {/* <div className={styles.media_logos}>
            <a href='https://www.skype.com/' target='_blank' rel='noreferrer'>
              <img src={skype} alt='skype' />
            </a>
            <a href='https://www.google.com' target='_blank' rel='noreferrer'>
              <img src={google_plus} alt='google-plus' />
            </a>
            <a href='https://www.facebook.com' target='_blank' rel='noreferrer'>
              <img src={facebook} alt='facebook' />
            </a>
            <a href='https://www.linkedin.com' target='_blank' rel='noreferrer'>
              <img src={linkedin} alt='linkedin' />
            </a>
            <a href='https://www.twitter.com' target='_blank' rel='noreferrer'>
              <img src={twitter} alt='twitter' />
            </a>
          </div> */}
        </div>
      </div>
    </div>
  );
};
export default Footer;
