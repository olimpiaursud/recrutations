import React from 'react';
import Footer from './footer';
import Header from './header';
import styles from './layout.module.scss';
import Helmet from 'react-helmet';
import { useSiteMetadata } from '../../hooks/metadata';
import '../../styles/global.scss';

const Layout = (props) => {
  const { title, description } = useSiteMetadata();

  return (
    <div className={styles.layout}>
      {/* Metadata query data */}
      <Helmet>
        <title>{title}</title>
        <script>
          {`!function(e){"use strict";function s(s){if(s){var t=e.documentElement;t.classList?t.classList.add("webp"):t.className+=" webp",window.sessionStorage.setItem("webpSupport",!0)}}!function(e){if(window.sessionStorage&&window.sessionStorage.getItem("webpSupport"))s(!0);else{var t=new Image;t.onload=t.onerror=function(){e(2===t.height)},t.src="data:image/webp;base64,UklGRi4AAABXRUJQVlA4TCEAAAAvAUAAEB8wAiMwAgSSNtse/cXjxyCCmrYNWPwmHRH9jwMA"}}(s)}(document);`}
        </script>
        <meta name='description' content={description} />
      </Helmet>
      {/* Header */}
      <Header {...props} />
      {/* Content */}
      <div className={styles.content}>{props.children}</div>
      {/* Footer */}
      <Footer />
    </div>
  );
};

export default Layout;
