/* slideshow */
import electrician_1 from './electrician_1.jpg';
import electrician_2 from './electrician_2.jpg';
import field from './field.jpg';
import industry from './industry.jpg';
import medical from './medical.jpg';
import nurse_medical from './nurse_medical.jpg';
import nurse from './nurse.jpg';
import old_people from './old_people.jpg';
import plumbing from './plumbing.jpg';
import painting_man from './painting_man.jpg';
import wine from './wine.jpg';
import working_field from './working_field.jpg';
import welder_1 from './welder_1.jpg';
import welder_2 from './welder_2.jpg';

export {
  electrician_1,
  electrician_2,
  field,
  medical,
  nurse,
  nurse_medical,
  industry,
  old_people,
  plumbing,
  painting_man,
  wine,
  working_field,
  welder_1,
  welder_2,
};
