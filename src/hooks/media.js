import { graphql, useStaticQuery } from 'gatsby';

const useMedia = () => {
  const query = useStaticQuery(
    graphql`
      query {
        images: allFile(filter: { sourceInstanceName: { eq: "media" } }) {
          edges {
            node {
              id
              relativePath
              extension
              publicURL
              childImageSharp {
                fluid(maxWidth: 1000) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                }
              }
            }
          }
        }
      }
    `
  );

  return query.images.edges;
  //   return null;
};

const useImage = (path) => {
  const image = useMedia().find((image) => image.node.relativePath === path);

  return (
    image &&
    image.node &&
    image.node.childImageSharp &&
    image.node.childImageSharp.fluid
  );
};

export { useMedia, useImage };
