import { graphql, useStaticQuery } from 'gatsby';

const useImages = () => {
  const query = useStaticQuery(
    graphql`
      query {
        allFile(
          filter: {
            sourceInstanceName: { eq: "images" }
            extension: { eq: "jpg" }
          }
        ) {
          edges {
            node {
              id
              relativePath
              childImageSharp {
                fluid(maxWidth: 1000) {
                  ...GatsbyImageSharpFluid_withWebp_noBase64
                }
              }
            }
          }
        }
      }
    `
  );

  return query.allFile.edges;
};

const useImage = (path) => {
  const image = useImages().find((image) => image.node.relativePath === path);

  return image && image.node && image.node.childImageSharp.fluid;
};

export { useImages, useImage };
