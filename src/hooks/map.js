import ReactMapboxGl from 'react-mapbox-gl';

const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1Ijoib2xpbXBpYWRlbmlzYSIsImEiOiJja2xzM25zYTIxdDBqMm9uMzh4dmwyeHJ5In0.hOTCszbGIe9ius78lgvLqA',
});

export { Map };
