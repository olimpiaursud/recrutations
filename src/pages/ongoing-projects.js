// @ts-nocheck
import * as React from 'react';
import Layout from '../components/layout/layout';
import styles from '../styles/projects.module.scss';
import { useImage } from '../hooks/media';
import Img from 'gatsby-image';
import { GoPrimitiveDot } from 'react-icons/go';
import { FormattedMessage } from 'gatsby-plugin-intl';

const OngoingProjectsPage = (props) => {
  return (
    <Layout {...props}>
      <div className={styles.story}>
        <div className={styles.text}>
          <div>
            <h1>
              <FormattedMessage id='projectsPage.academy' />
            </h1>
          </div>
          <p>
            <GoPrimitiveDot />
            <FormattedMessage id='projectsPage.first' />
          </p>
          <p>
            <GoPrimitiveDot />
            <FormattedMessage id='projectsPage.second' />
          </p>
          <p>
            <GoPrimitiveDot />
            <FormattedMessage id='projectsPage.third' />
          </p>
          <p>
            <GoPrimitiveDot />
            <FormattedMessage id='projectsPage.forth' />
          </p>
        </div>
        <Img
          fadeIn={false}
          loading={'eager'}
          className={styles.image}
          fluid={useImage('nurses.jpg')}
          alt='nurse'
        />
      </div>
    </Layout>
  );
};

export default OngoingProjectsPage;
