import * as React from 'react';
import Layout from '../components/layout/layout';
// @ts-ignore
import styles from './../styles/index.module.scss';
import Carousel from 'react-bootstrap/Carousel';
import CountUp from 'react-countup';
import { FlashSharp } from 'react-ionicons';
import { useImage, useImages } from '../hooks/images';
import Img from 'gatsby-image';
import 'mapbox-gl/dist/mapbox-gl.css';
import { FormattedMessage, Link } from 'gatsby-plugin-intl';
import { useImage as useMedia } from '../hooks/media';
import { FaMapMarkerAlt } from 'react-icons/fa';
import FadeInSection from '../components/fade-in/fade-in';
import { useIntl } from 'gatsby-plugin-intl';

const IndexPage = (props) => {
  const carousel_images = useImages();
  const intl = useIntl();

  return (
    <Layout {...props}>
      <div className={styles.container}>
        <div className={styles.slideshow}>
          <Carousel interval={1500} keyboard>
            {carousel_images.map(
              (image) =>
                image &&
                image.node.childImageSharp &&
                image.node.childImageSharp.fluid && (
                  <Carousel.Item key={image.node.id}>
                    <Img
                      fadeIn={false}
                      loading={'eager'}
                      className={styles.img}
                      fluid={
                        image.node.childImageSharp &&
                        image.node.childImageSharp.fluid
                      }
                      alt={'slideshow'}
                    />
                  </Carousel.Item>
                )
            )}
          </Carousel>
          <div className={styles.text}>
            <h1>
              <FormattedMessage id='motto' />
            </h1>
          </div>
        </div>
        <div className={styles.categories}>
          <Link to='/qualified-workers'>
            <div className={styles.card}>
              <Img
                fadeIn={false}
                loading={'eager'}
                className={styles.image}
                fluid={useImage('electrician_2.jpg')}
                alt='crafts'
              />
              <div className={styles.description}>
                <h3>
                  <FormattedMessage id='qualified_workers' />
                </h3>
                <p>
                  <FormattedMessage id='qualified_workers_types' />
                </p>
              </div>
            </div>
          </Link>
          <Link to='/industry'>
            <div className={styles.card}>
              <Img
                fadeIn={false}
                loading={'eager'}
                className={styles.image}
                fluid={useImage('industry.jpg')}
                alt='industry'
              />
              <div className={styles.description}>
                <h3>
                  <FormattedMessage id='industry' />
                </h3>
                <p>
                  <FormattedMessage id='factory_workers' />
                </p>
              </div>
            </div>
          </Link>
          <Link to='/medicine'>
            <div className={styles.card}>
              <Img
                fadeIn={false}
                loading={'eager'}
                className={styles.image}
                fluid={useImage('slide3.jpg')}
                alt='medicine & nursing'
              />
              <div className={styles.description}>
                <h3>
                  <FormattedMessage id='medicine_id' />
                </h3>
                <p>
                  <FormattedMessage id='care_people' />
                </p>
              </div>
            </div>
          </Link>
          <Link to='/agriculture'>
            <div className={styles.card}>
              <Img
                fadeIn={false}
                loading={'eager'}
                className={styles.image}
                fluid={useImage('working_field.jpg')}
                alt='agriculture'
              />
              <div className={styles.description}>
                <h3>
                  <FormattedMessage id='agriculture' />
                </h3>
                <p>
                  <FormattedMessage id='farm_workers' />
                </p>
              </div>
            </div>
          </Link>
        </div>
      </div>

      <FadeInSection>
        <div className={styles.map}>
          <div>
            <h2>
              <FormattedMessage id='best_people_all_needs' />
            </h2>
            <h3>
              <FormattedMessage id='all_germany' />
            </h3>
            <h4>
              <FormattedMessage id='our_partners' />
            </h4>
            <div className={styles.locations}>
              <p>
                <FaMapMarkerAlt /> <FormattedMessage id='countries.berlin' />
              </p>
              <p>
                <FaMapMarkerAlt /> <FormattedMessage id='countries.london' />
              </p>
              <p>
                <FaMapMarkerAlt /> <FormattedMessage id='countries.stockholm' />
              </p>
              <p>
                <FaMapMarkerAlt /> <FormattedMessage id='countries.oslo' />
              </p>
              <p>
                <FaMapMarkerAlt /> <FormattedMessage id='countries.bruxelles' />
              </p>
              <p>
                <FaMapMarkerAlt /> <FormattedMessage id='countries.praga' />
              </p>
              <p>
                <FaMapMarkerAlt /> <FormattedMessage id='countries.amsterdam' />
              </p>
            </div>
          </div>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.map_image}
            fluid={useMedia(
              intl.locale === 'en'
                ? 'germany - en.png'
                : intl.locale === 'ge'
                ? 'germany - de.png'
                : 'germany-ro.png'
            )}
            alt='harta'
          />
        </div>
      </FadeInSection>
      <div className={styles.numbers}>
        <div className={styles.background}></div>

        <div className={styles.circles}>
          <div className={styles.circle}>
            <FlashSharp color={'#00000'} />
            <CountUp end={500} duration={1} />
            <p>
              <FormattedMessage id='job_offers' />
            </p>
          </div>
          <div className={styles.circle}>
            <FlashSharp color={'#00000'} />
            <CountUp
              start={10000}
              end={12000}
              duration={1000}
              onEnd={({ start }) => start()}
            />
            <p>
              <FormattedMessage id='employed' />
            </p>
          </div>
          <div className={styles.circle}>
            <FlashSharp color={'#00000'} />
            <CountUp end={150} duration={1} />
            <p>
              <FormattedMessage id='happy_companies' />
            </p>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default IndexPage;
