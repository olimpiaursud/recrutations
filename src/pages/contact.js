import * as React from 'react';
import Layout from '../components/layout/layout';
import styles from '../styles/contact.module.scss';
import stylesCategory from '../styles/category.module.scss';
import 'mapbox-gl/dist/mapbox-gl.css';
import { useImage } from '../hooks/media';
import Img from 'gatsby-image';
import { FormattedMessage } from 'gatsby-plugin-intl';
import ReactMapGL, { Marker } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

const ContactPage = (props) => {
  return (
    <Layout {...props}>
      <div className={stylesCategory.header}>
        <div className={stylesCategory.description}>
          <h3>
            <FormattedMessage id='contact' />
          </h3>
        </div>
        <Img
          fadeIn={false}
          loading={'eager'}
          className={stylesCategory.image}
          fluid={useImage('handshake.jpg')}
          alt='contact'
        />
      </div>
      <div className={styles.contact}>
        <div className={styles.info}>
          {/* <p>
            <FormattedMessage id='phone' />
          </p> */}
          <p>Cristina Mihai</p>
          <a target='_blank' rel='noreferrer' href='tel:+40 740 766 602'>
            +40 740 766 602
          </a>
          {/* <p>
            <FormattedMessage id='phone' />
          </p> */}
          <p>Oana Stan</p>
          <a target='_blank' rel='noreferrer' href='tel:+40 726 526 385'>
            +40 726 526 385
          </a>
          <p>
            <FormattedMessage id='mail' />
          </p>
          <a href='mailto:office@perspective4you.com'>
            office@perspective4you.com
          </a>

          <p>
            <FormattedMessage id='address' />
          </p>
          <a
            target='_blank'
            rel='noreferrer'
            href='https://www.google.com/maps/place/Strada Harmanului 50, Brașov, Romania'
          >
            <FormattedMessage id='street' />
          </a>
        </div>
        <div className={styles.map}>
          {/* <Map
            style={'mapbox://styles/mapbox/streets-v9'}
            containerStyle={{
              height: '35vh',
              width: '100%',
              borderRadius: '10px',
              outline: 'none',
            }}
            center={[25.616575241088867, 45.65529251098633]}
          >
            <Marker
              coordinates={[25.616575241088867, 45.65529251098633]}
              anchor='bottom'
            >
              <div className='mapMarkerStyle'></div>
            </Marker>
          </Map> */}
          <ReactMapGL
            mapboxApiAccessToken={
              'pk.eyJ1Ijoib2xpbXBpYWRlbmlzYSIsImEiOiJja2xzM25zYTIxdDBqMm9uMzh4dmwyeHJ5In0.hOTCszbGIe9ius78lgvLqA'
            }
            mapStyle='mapbox://styles/mapbox/streets-v11'
            latitude={45.65447283529362}
            longitude={25.616240622718724}
            zoom={17}
            height='35vh'
            width='100%'
            style={{ borderRadius: '10px', outline: 'none' }}
          >
            {
              <Marker
                latitude={45.65447283529362}
                longitude={25.616240622718724}
              >
                <div
                  className={styles.marker + ' ' + styles['temporary-marker']}
                >
                  <span></span>
                </div>
              </Marker>
            }
            {/* {this.state.markers.map((marker, index) => {
              return (
                <CustomMarker
                  key={`marker-${index}`}
                  index={index}
                  marker={marker}
                />
              );
            })} */}
          </ReactMapGL>
          {/* <Img
            fadeIn={false}
            loading={'eager'}
            className={stylesCategory.image}
            fluid={useImage('location.png')}
            alt='contact'
          /> */}
        </div>
      </div>
    </Layout>
  );
};

export default ContactPage;
