import { Link } from 'gatsby';
import * as React from 'react';
import Layout from '../components/layout/layout';
import { useImage } from '../hooks/images';
import styles from '../styles/working-fields.module.scss';
import Img from 'gatsby-image';
import { FormattedMessage } from 'gatsby-plugin-intl';

const WorkingFieldsPage = (props) => {
  return (
    <Layout {...props}>
      <div className={styles.categories}>
        <Link className={styles.category} to='/qualified-workers'>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('electrician_2.jpg')}
            alt='technicians'
          />
          <div className={styles.text}>
            <h3>
              <FormattedMessage id='qualified_workers' />
            </h3>
            <h5>
              <FormattedMessage id='qualified_workers_types' />
            </h5>
            <p>
              <FormattedMessage id='qworkersPage.first' />
            </p>
          </div>
        </Link>
        <Link className={styles.category} to='/industry'>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('industry.jpg')}
            alt='industry'
          />
          <div className={styles.text}>
            <h3>
              <FormattedMessage id='industry' />
            </h3>
            <h5>
              <FormattedMessage id='factory_workers' />
            </h5>
            <p>
              <FormattedMessage id='industryPage.first' />
            </p>
          </div>
        </Link>
        <Link className={styles.category} to='/medicine'>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('slide3.jpg')}
            alt='medicine & nursing'
          />
          <div className={styles.text}>
            <h3>
              <FormattedMessage id='medicine' />
            </h3>
            <h5>
              <FormattedMessage id='care_people' />
            </h5>
            <p>
              <FormattedMessage id='medicinePage.first' />
            </p>
          </div>
        </Link>
        <Link className={styles.category} to='/agriculture'>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('working_field.jpg')}
            alt='agriculture'
          />
          <div className={styles.text}>
            <h3>
              <FormattedMessage id='agriculture' />
            </h3>
            <h5>
              <FormattedMessage id='farm_workers' />
            </h5>
            <p>
              <FormattedMessage id='agriculturePage.first' />
            </p>
          </div>
        </Link>
      </div>
    </Layout>
  );
};

export default WorkingFieldsPage;
