import * as React from 'react';
import Layout from '../components/layout/layout';
import styles from '../styles/category.module.scss';
import Img from 'gatsby-image';
import { useImage } from '../hooks/images';
import { FormattedMessage } from 'gatsby-plugin-intl';
import { FaCheck } from 'react-icons/fa';

const Industry = (props) => {
  return (
    <Layout {...props}>
      <div className={styles.header}>
        <div className={styles.description}>
          <h3>
            <FormattedMessage id='industry' />
          </h3>
          <p>
            <FormattedMessage id='factory_workers' />
          </p>
        </div>
        <Img
          fadeIn={false}
          loading={'eager'}
          className={styles.image}
          fluid={useImage('industry.jpg')}
          alt='industry'
        />
      </div>
      <div className={styles.text}>
        <p>
          <FormattedMessage id='industryPage.first' />
        </p>

        <p>
          <FormattedMessage id='industryPage.second' />
        </p>

        <p>
          <FaCheck /> <FormattedMessage id='industryPage.third' />
        </p>

        <p>
          <FaCheck /> <FormattedMessage id='industryPage.forth' />
        </p>

        <p>
          <FaCheck /> <FormattedMessage id='industryPage.fifth' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='industryPage.sixth' />
        </p>
        <p>
          <FormattedMessage id='industryPage.seventh' />
        </p>
      </div>
    </Layout>
  );
};

export default Industry;
