import * as React from 'react';
import Layout from '../components/layout/layout';
import { useImage } from '../hooks/media';
import styles from '../styles/category.module.scss';
import Img from 'gatsby-image';
import { FormattedMessage } from 'gatsby-plugin-intl';
import { FaCheck } from 'react-icons/fa';

const Medicine = (props) => {
  return (
    <Layout {...props}>
      <div className={styles.header}>
        <div className={styles.description}>
          <h3>
            <FormattedMessage id='medicine' />
          </h3>
          <p>
            <FormattedMessage id='care_people' />
          </p>
        </div>
        <Img
          fadeIn={false}
          loading={'eager'}
          className={styles.image}
          fluid={useImage('nurse.jpg')}
          alt='medicine & nursing'
        />
      </div>
      <div className={styles.text}>
        <p>
          <FormattedMessage id='medicinePage.first' />
        </p>
        <p>
          <FormattedMessage id='medicinePage.second' />
        </p>

        <p>
          <FaCheck /> <FormattedMessage id='medicinePage.third' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='medicinePage.forth' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='medicinePage.fifth' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='medicinePage.sixth' />
        </p>
        <p>
          <FormattedMessage id='medicinePage.seventh' />
        </p>
      </div>
    </Layout>
  );
};

export default Medicine;
