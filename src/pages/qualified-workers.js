import * as React from 'react';
import Layout from '../components/layout/layout';
import styles from '../styles/category.module.scss';
import Img from 'gatsby-image';
import { useImage } from '../hooks/images';
import { FormattedMessage } from 'gatsby-plugin-intl';
import { FaCheck } from 'react-icons/fa';

const Crafts = (props) => {
  return (
    <Layout {...props}>
      <div className={styles.header}>
        <div className={styles.description}>
          <h3>
            <FormattedMessage id='qualified_workers' />
          </h3>
          <p>
            <FormattedMessage id='qualified_workers_types' />
          </p>
        </div>
        <Img
          fadeIn={false}
          loading={'eager'}
          className={styles.image}
          fluid={useImage('electrician_2.jpg')}
          alt='crafts'
        />
      </div>
      <div className={styles.text}>
        <p>
          <FormattedMessage id='qworkersPage.first' />
        </p>
        <p>
          <FormattedMessage id='qworkersPage.second' />
        </p>

        <p>
          <FaCheck /> <FormattedMessage id='qworkersPage.third' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='qworkersPage.forth' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='qworkersPage.fifth' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='qworkersPage.sixth' />
        </p>

        <p>
          <FormattedMessage id='qworkersPage.seventh' />
        </p>
      </div>
    </Layout>
  );
};

export default Crafts;
