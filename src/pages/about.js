import * as React from 'react';
import Layout from '../components/layout/layout';
import styles from '../styles/about.module.scss';
import { useImage } from '../hooks/media';
import Img from 'gatsby-image';
import FadeInSection from '../components/fade-in/fade-in';
import {
  FaArrowAltCircleRight,
  FaArrowCircleRight,
  FaCheck,
} from 'react-icons/fa';
import { FormattedMessage } from 'gatsby-plugin-intl';

const AboutPage = (props) => {
  return (
    <Layout {...props}>
      <FadeInSection>
        <div className={styles.section}>
          <div className={styles.text}>
            <p>
              <FaArrowCircleRight />
              <FormattedMessage id='aboutPage.first' />
            </p>
            <p>
              <FaArrowCircleRight />
              <FormattedMessage id='aboutPage.second' />
            </p>
            <p>
              <FaArrowCircleRight />
              <FormattedMessage id='aboutPage.third' />
            </p>
          </div>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('director-worker.png')}
            alt='contract'
          />
        </div>
      </FadeInSection>
      <FadeInSection>
        <div className={styles.reversed_section}>
          <div className={styles.text}>
            <p>
              <FaArrowCircleRight />
              <FormattedMessage id='aboutPage.forth' />
            </p>
            <p>
              <FaArrowCircleRight />
              <FormattedMessage id='aboutPage.fifth' />
            </p>
          </div>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('industry_exemple.jpg')}
            alt='contract'
          />
        </div>
      </FadeInSection>
      <FadeInSection>
        <div className={styles.section}>
          <div className={styles.text}>
            <h3>
              <FormattedMessage id='aboutPage.areas' />
            </h3>
            <div className={styles.enum}>
              <p>
                <FaCheck /> <FormattedMessage id='aboutPage.qualified' />
              </p>
              <p>
                <FaCheck /> <FormattedMessage id='aboutPage.industry' />
              </p>
              <p>
                <FaCheck /> <FormattedMessage id='aboutPage.agriculture' />
              </p>

              <p>
                <FaCheck /> <FormattedMessage id='aboutPage.medical' />
              </p>
            </div>
          </div>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('nurse_exemple.jpg')}
            alt='testing'
          />
        </div>
      </FadeInSection>

      <FadeInSection>
        <div className={styles.section_special}>
          <div className={styles.text}>
            <h2>
              <FormattedMessage id='aboutPage.advantages' />
            </h2>
            <p>
              <FormattedMessage id='aboutPage.benefits' />
            </p>
            <div className={styles.categories}>
              <div>
                <h5>
                  <FormattedMessage id='aboutPage.focus' />
                </h5>
                <p>
                  <FormattedMessage id='aboutPage.specialize' />
                </p>
              </div>
              <div>
                <h5>
                  <FormattedMessage id='aboutPage.specialization' />
                </h5>
                <p>
                  <FormattedMessage id='aboutPage.developed' />
                </p>
              </div>
              <div>
                <h5>
                  <FormattedMessage id='aboutPage.guarantee' />
                </h5>
                <p>
                  <FormattedMessage id='aboutPage.maintain' />
                </p>
              </div>
              <div>
                <h5>
                  <FormattedMessage id='aboutPage.profit' />
                </h5>
                <p>
                  <FormattedMessage id='aboutPage.makeSure' />
                </p>
              </div>
            </div>
          </div>
        </div>
      </FadeInSection>
      <FadeInSection>
        <div className={styles.reversed_section}>
          <div className={styles.text}>
            <h2>
              <FormattedMessage id='aboutPage.obiective' />
            </h2>
            <p>
              <FaArrowAltCircleRight />
              <FormattedMessage id='aboutPage.sixth' />
            </p>
            <p>
              <FaArrowAltCircleRight />
              <FormattedMessage id='aboutPage.seventh' />
            </p>
          </div>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('handshake 2.jpg')}
            alt='arrow'
          />
        </div>
      </FadeInSection>
      <FadeInSection>
        <div className={styles.section}>
          <div className={styles.text}>
            <h4>
              <FormattedMessage id='aboutPage.weGuarantee' />
            </h4>
            <p>
              <FaArrowAltCircleRight />{' '}
              <FormattedMessage id='aboutPage.experience' />
            </p>
            <p>
              <FaArrowAltCircleRight />{' '}
              <FormattedMessage id='aboutPage.multilingual' />
            </p>
            <p>
              <FaArrowAltCircleRight />{' '}
              <FormattedMessage id='aboutPage.communication' />
            </p>
            <p>
              <FaArrowAltCircleRight />{' '}
              <FormattedMessage id='aboutPage.knowledge' />
            </p>
            <p>
              <FaArrowAltCircleRight />{' '}
              <FormattedMessage id='aboutPage.cultural' />
            </p>
            <p>
              <FaArrowAltCircleRight />{' '}
              <FormattedMessage id='aboutPage.infoLine' />
            </p>
          </div>
          <Img
            fadeIn={false}
            loading={'eager'}
            className={styles.image}
            fluid={useImage('flag.jpg')}
            alt='flag'
          />
        </div>
      </FadeInSection>
    </Layout>
  );
};

export default AboutPage;
