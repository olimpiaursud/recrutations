import * as React from 'react';
import Layout from '../components/layout/layout';
import { useImage } from '../hooks/images';
import styles from '../styles/category.module.scss';
import Img from 'gatsby-image';
import { FormattedMessage } from 'gatsby-plugin-intl';
import { FaCheck } from 'react-icons/fa';

const Agriculture = (props) => {
  return (
    <Layout {...props}>
      <div className={styles.header}>
        <div className={styles.description}>
          <h3>
            <FormattedMessage id='agriculture' />
          </h3>
          <p>
            <FormattedMessage id='farm_workers' />
          </p>
        </div>
        <Img
          fadeIn={false}
          loading={'eager'}
          className={styles.image}
          fluid={useImage('field.jpg')}
          alt='agriculture'
        />
      </div>
      <div className={styles.text}>
        <p>
          <FormattedMessage id='agriculturePage.first' />
        </p>
        <p>
          <FormattedMessage id='agriculturePage.second' />
        </p>

        <p>
          <FaCheck /> <FormattedMessage id='agriculturePage.third' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='agriculturePage.forth' /> –
          <FormattedMessage id='agriculturePage.fifth' />
        </p>
        <p>
          <FaCheck /> <FormattedMessage id='agriculturePage.sixth' />
        </p>

        <p>
          <FormattedMessage id='agriculturePage.seventh' />
        </p>
      </div>
    </Layout>
  );
};

export default Agriculture;
