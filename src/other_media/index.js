/* header & footer logos */
import logo from './logo.jpg';
import facebook from './facebook.svg';
import skype from './skype.svg';
import google_plus from './google-plus.svg';
import linkedin from './linkedin.svg';
import twitter from './twitter.svg';

export { logo, facebook, skype, google_plus, linkedin, twitter };
